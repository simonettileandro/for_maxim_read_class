# !/usr/bin/env python
# By Leandro Simonetti
# Shared under XX
#
# Script:       NGS_table_class
# Version:      1
# Last Edit:    2018-12-18
desc_text= '''
The class represents/handles the "NGS_table" for NGS demultiplexing.
It's main use is in the NGS_demultiplexing script, but can be used by itself
to test the errors on an NGS_table.
To instantiate, the NGS_table in .csv/.tsv/.tdt is needed.
'''
# Functions:
#
# Objects:
#     NGS_table
# Commandline:
#     pythpn NGS_table_class.py <path_to_NGS_table> [--separator <auto/tab/,/etc>]
# Dependencies:
#     General: sys, os, argparse, re, copy, biopython
#     Custom:


## MODULES
import sys, os, argparse, re
from copy import deepcopy
from Bio.Seq import Seq


## CLASSES
class NGS_table(object):
    # Attributes
    # Shared attributes (constants)
    # -from the NGS_table
    _for_bc_col     = 'Barcode FOR'
    _rev_bc_col     = 'Barcode REV'
    _plate_col      = 'Plate'
    _well_col       = 'Well'
    _researcher_col = 'Researcher Name'
    _rev_oligo_col  = 'Reverse?'
    _prot_id_col    = 'Protein ID'
    _gene_name_col  = 'Gene Name'
    _lib_id_col     = 'Library ID'
    _experiment_col = 'Experiment Replica Nbr.'
    _slection_col   = 'Selection Day'
    # -from the Libraries_summary_table
    _for_pr_col     = 'forward_primer'
    _rev_pr_col     = 'reverse_primer'
    # Shared mutable attributes (constants)
    # -for cleaning tables
    _empty_cell_values = ['0', '#N/A', '#VALUE!']
    # -for generating output names
    _naming_cols_data  = ['Plate', 'Well', 'Researcher Name',
                          'Gene Name', 'Domain Name', 'Domain Number(s)',
                          'UP Accession', 'Start', 'End',
                          'Library ID',
                          'Selection Day',
                          ]
    _naming_columns    = ['Plate', 'Well',
                          'Protein ID',
                          'Library ID',
                          'Selection Day',
                          ]
    _replace_from_to   = [# replace by dash
                          ('_', '-'),
                          (' ', '-'),
                          ("/", '-'),
                          ('(', '-'),
                          (')', '-'),
                          # just remove
                          ('+', ''),
                          ('.', ''),
                          (',', ''),
                          ("'", ''),
                          ('"', ''),
                          (":", ''),
                          (";", ''),
                          ("=", ''),
                          ("?", ''),
                          ("!", ''),
                          ]

    # Methods
    def __init__(self, path_to_table, unique_id, libraries_summary_dict=None,
                 separator='auto', graphic_format='png', log=None,
                 let_me_be=False):
        """
        About some of the optional arguments:
        libraries_summary_dict -> the dictionary from the libraries summary
            table, the only thing needed from here are the primers
        separator -> can be "auto/tab/,/etc"
        graphic_format -> from pyplot (png/tif/svg/etc)
        log -> path to a log txt file, if None, the info only prints to screen
        let_me_be -> doesn't stop running if the NGS_barcodes table rises erros;
            It will print the errors to screen [and to log], though (it's
            useful if you want to run the demultiplexing or the combine script
            even with errors)
        """
        # create attributes
        self._path = path_to_table
        self.unique_id = unique_id
        self._out_folder = '{}_OUTPUT'.format(self.unique_id)
        self._separator = separator
        self._log = log
        self._let_me_be = let_me_be
        # create mutable attributes
        self._libraries_dict = libraries_summary_dict
        self._barcodes_tree = {}
        self._barcodes_tree_rev = {}
        self._table = []
        self._head = []
        self._file_ends  = {'dna'       : '_DNA.fasta',
                            'dna_count' : '_DNA_COUNTS.tsv',
                            'dna_plot'  : '_DNA_PLOTS.{}'.format(graphic_format),
                            'prot_count': '_PEPTIDE_COUNTS.tsv',
                            'lib_table' : '_IN_LIB_Table.tsv',
                            'lib_plot'  : '_inLIB_PLOT.{}'.format(graphic_format),
                            'combine'   : '_COMBINED.tsv',
                            }
        self._colname2index_dict = {}
        self.libraries_in_table = []
        self.researchers_in_table = []
        self.protein_ids_in_table = []
        self._for_barcodes_in_table = []
        self._rev_barcodes_in_table = []
        self._for_primers_in_table = []
        self._rev_primers_in_table = []
        self._counts = {}
        # load and process files
        self._load_and_check()
        self._process()

    def info(self):
        """
        Outputs instance information
        """
        self._print_log('NGS_table: {}'.format(os.path.basename(self._path)))
        self._print_log('{} researchers found:'.format(len(self.researchers_in_table)))
        for r in self.researchers_in_table:
            self._print_log('\t{} -> {} rows'.format(r, self._counts[r]))
        self._print_log('{} libraries found:'.format(len(self.libraries_in_table)))
        for l in self.libraries_in_table:
            self._print_log('\t{} -> {} rows'.format(l, self._counts[l]))
        self._print_log('Good/bad rows:')
        self._print_log('\tOK_rows -> {} rows'.format(self._rows_with_data))
        self._print_log('\tSKIPPED_rows -> {} rows'.format(self._skipped_rows))
        self._print_log('\tERROR_rows -> {} rows'.format(self._big_errors))

    def _load_and_check(self):
        """
        Loads the NGS_table to the list self._table and checks rows and
        cells for needed content + fixes some situations:
            IF no researcher -> set to 'NoName'
            IF cell value in self._empty_cell_values -> set to ''
        """
        # check if there is a libraries_summary_table
        if not self._libraries_dict:
            self._print_log('WARNING! No "libraries_summary_dict" provided, the primers will be set to "None"')
        # now check the table for errors
        self._rows_with_data = 0
        self._skipped_rows = 0
        self._big_errors = 0
        with open(self._path) as table:
            # get the header and the separator
            self._head = table.readline().strip('\n')
            self._get_separator()
            self._head = self._head.split(self._separator)
            # get the indexes and names of the columns
            self._colname2index_dict = dict(zip(self._head,
                                           range(len(self._head))
                                           ))
            # self._colindex2names_dict = dict(zip(range(len(self._head)),
            #                                  self._head
            #                                  ))
            for row in table:
                # separate the row
                row = row.strip('\n').split(self._separator)
                # clean the empty cells
                for value in self._empty_cell_values:
                    while value in row:
                        row[row.index(value)] = ''
                if not row[self._colname2index_dict[self._researcher_col]]:
                    row[self._colname2index_dict[self._researcher_col]] = 'NoName'
                # extract values
                lib_id = row[self._colname2index_dict[self._lib_id_col]]
                for_bc = row[self._colname2index_dict[self._for_bc_col]]
                rev_bc = row[self._colname2index_dict[self._rev_bc_col]]
                prot_id = row[self._colname2index_dict[self._prot_id_col]]
                # check that the needed fields have data
                if lib_id and for_bc and rev_bc and prot_id:
                    gene_id = row[self._colname2index_dict[self._gene_name_col]]
                    if gene_id and gene_id != '#REF!':
                        self._rows_with_data += 1
                        # add the cleaned row to the table
                        self._table.append(row)
                    else:
                        self._print_log('ERROR! Row: "{}" (has no gene_id)'.format(' | '.join(row[:3] + row[8:9])))
                        self._big_errors += 1
                        if self._let_me_be:
                            self._table.append(row)
                else:
                    skip = []
                    if not lib_id:
                        skip.append('lib_id')
                    if not for_bc or not rev_bc:
                        skip.append('barcodes')
                    if not prot_id:
                        skip.append('prot_id')
                    self._print_log('SKIPPED row: "{}" (lacks {})'.format(' | '.join(row[:3] + row[8:9]), ', '.join(skip).strip()))
                    self._skipped_rows += 1
        if self._big_errors:
            msg = 'ERROR! {} produced critical errors in table "{}"!'.format('One row' if self._big_errors == 1 else '{} rows'.format(self._big_errors), os.path.basename(self._path))
            self._print_log(msg, True)
            if not self._let_me_be:
                raise ImportError(msg)

    def _process(self):
        """
        Parse the self._table info to dictionaries, generate the paths to files
        """
        # generate the output folder
        complete_out_path = '{}/{}'.format(os.path.dirname(self._path),
                                           self._out_folder)
        # if the libraries_summary data is not provided, we need to come up
        # with som primers that will not display errors
        if not self._libraries_dict:
            fake_primers = 0
        for row in self._table:
            # generate the dictionary from the row
            row_dict = {}
            for k in self._head:
                row_dict[k] = row[self._colname2index_dict[k]]
            # fix the reverse str -> bool
            row_dict[self._rev_oligo_col] = True \
                                            if self._rev_oligo_col == 'TRUE' \
                                            else False
            # get the barcodes and primers
            lib_id = row_dict[self._lib_id_col]
            researcher = row_dict[self._researcher_col]
            prot_id = row_dict[self._prot_id_col]
            for_bc = row_dict[self._for_bc_col]
            rev_bc = row_dict[self._rev_bc_col]
            if self._libraries_dict:
                for_pr = self._libraries_dict[lib_id][self._for_pr_col]
                rev_pr = self._libraries_dict[lib_id][self._rev_pr_col]
            else:
                for_pr = rev_pr = 'None_{}'.format(fake_primers)
                fake_primers += 1
            # bc_n_pr = for_bc + for_pr + rev_pr + rev_bc
            # add the primers to the dictionary
            row_dict[self._for_pr_col] = for_pr
            row_dict[self._rev_pr_col] = rev_pr
            # add the file_names
            res_name = self._make_file_name(row, self._naming_columns)
            res_name = '{}/{}/{}'.format(complete_out_path, researcher, res_name)
            row_dict['researcher_filename'] = res_name
            row_dict['researcher_filename_abs'] = os.path.abspath(res_name)
            data_name = self._make_file_name(row, self._naming_cols_data)
            data_name = '{}/Data/{}'.format(complete_out_path, data_name)
            row_dict['data_filename'] = data_name
            row_dict['data_filename_abs'] = os.path.abspath(data_name)
            # generate a fixed version of the Protein ID, with the changes
            # applied to build the names
            row_dict['fixed_protein_id'] = self._make_file_name(row,
                                                                [self._prot_id_col],
                                                                False
                                                                )
            # generate the barcodes+primers tree
            if for_bc not in self._barcodes_tree:
                self._barcodes_tree[for_bc] = {}
            if for_pr not in self._barcodes_tree[for_bc]:
                self._barcodes_tree[for_bc][for_pr] = {}
            if rev_pr not in self._barcodes_tree[for_bc][for_pr]:
                self._barcodes_tree[for_bc][for_pr][rev_pr] = {}
            if rev_bc not in self._barcodes_tree[for_bc][for_pr][rev_pr]:
                self._barcodes_tree[for_bc][for_pr][rev_pr][rev_bc] = deepcopy(row_dict)
            else:
                msg = 'This should not be possible, same barcodes and primers for different experiments [{}]'.format(', '.join([for_bc, for_pr, rev_pr, rev_bc]))
                self._print_log(msg, True)
                raise IOError(msg)
            # generate the REVERSE barcodes+primer tree
            rc_rev_bc = str(Seq(rev_bc).reverse_complement())
            rc_rev_pr = str(Seq(rev_pr).reverse_complement())
            rc_for_pr = str(Seq(for_pr).reverse_complement())
            rc_for_bc = str(Seq(for_bc).reverse_complement())
            if rc_rev_bc not in self._barcodes_tree_rev:
                self._barcodes_tree_rev[rc_rev_bc] = {}
            if rc_rev_pr not in self._barcodes_tree_rev[rc_rev_bc]:
                self._barcodes_tree_rev[rc_rev_bc][rc_rev_pr] = {}
            if rc_for_pr not in self._barcodes_tree_rev[rc_rev_bc][rc_rev_pr]:
                self._barcodes_tree_rev[rc_rev_bc][rc_rev_pr][rc_for_pr] = {}
            if rc_for_bc not in self._barcodes_tree_rev[rc_rev_bc][rc_rev_pr][rc_for_pr]:
                self._barcodes_tree_rev[rc_rev_bc][rc_rev_pr][rc_for_pr][rc_for_bc] = self._barcodes_tree[for_bc][for_pr][rev_pr][rev_bc]
            # add the values to the (future to be) sets
            self.libraries_in_table.append(lib_id)
            self.researchers_in_table.append(researcher)
            self.protein_ids_in_table.append(prot_id)
            self._for_barcodes_in_table.append(for_bc)
            self._rev_barcodes_in_table.append(rev_bc)
            self._for_primers_in_table.append(for_pr)
            self._rev_primers_in_table.append(rev_pr)
            # generate extra stuff...
            # TODO!
        # count the rows for libraries and researchers
        for x in self.libraries_in_table:
            self._counts[x] = self.libraries_in_table.count(x)
        for x in self.researchers_in_table:
            self._counts[x] = self.researchers_in_table.count(x)
        # generate a sets of libraries, researchers, for and rev Barcodes
        self.libraries_in_table = list(set(self.libraries_in_table))
        self.researchers_in_table = list(set(self.researchers_in_table))
        self._for_barcodes_in_table = list(set(self._for_barcodes_in_table))
        self._rev_barcodes_in_table = list(set(self._rev_barcodes_in_table))
        self._for_primers_in_table = list(set(self._for_primers_in_table))
        self._rev_primers_in_table = list(set(self._rev_primers_in_table))
        self.protein_ids_in_table = list(set(self.protein_ids_in_table))
        self.libraries_in_table.sort()
        self.researchers_in_table.sort()
        self._for_barcodes_in_table.sort()
        self._rev_barcodes_in_table.sort()
        self._for_primers_in_table.sort()
        self._rev_primers_in_table.sort()
        self.protein_ids_in_table.sort()
        # generate the dictionary that maps protein_ID to files

        # generate extra stuff...
        # TODO!

    def _get_separator(self):
        """
        Gets the column separator for a text formatted table. First it tries to
        get it from the file extension (tsv/tdt or csv) and then by searching
        for them in the header
        """
        if self._separator == 'auto':
            if self._path.endswith('.tsv') or self._path.endswith('.tdt'):
                self._separator = '\t'
            elif self._path.endswith('.csv'):
                self._separator = ','
            else:
                line = self._head
                if '\t' in line:
                    self._separator = '\t'
                elif ',' in line:
                    self._separator = ','
                elif ';' in line:
                    self._separator = ';'
                else:
                    msg =  'Could not auto-detect the separator'
                    self._print_log(msg, True)
                    raise ImportError(msg)

    def _make_file_name(self, row, list_of_columns, add_unique_id=True):
        """self._make_file_name(list_of_columns) -> str
        Given a list of column names, it returns the file-name combining the
        contents of those columns and the unique_id
        """
        if add_unique_id:
            name = [self.unique_id]
        else:
            name = []
        for i in list_of_columns:
            i = row[self._colname2index_dict[i]]
            if len(i) > 0:
                for old, new in self._replace_from_to:
                    i = i.replace(old, new).strip()
                name.append(i)  # if you remove 1 tab, you get multi "_" and index consistency
        name = '_'.join(name).replace('-_', '_').replace('_-', '_').replace('--', '-')
        return name

    def _print_log(self, content, only_to_file=False):
        """self._print_log(str, bool) -> None
        Prints and saves the content into a path. If "only_to_file" it does
        not print to screen and just saves the line to the log file
        """
        if not only_to_file:
            print(content)
        if self._log:
            content += '\n'
            self._save2file(self._log, content)

    def get_next_level_values(self, for_bc=None, for_pr=None,
                              rev_pr=None, rev_bc=None,
                              reverse_complement=False):
        """self.get_next_level_values(args* [, bool]) -> list
        Returns the list of barcodes or primers corresponding to the next level
        of the data provided. For example: if a for_bc and a for_pr are provided
        the method will return the rev_pr for the combination of those values
        """
        if reverse_complement:
            if not rev_bc:
                return list(self._barcodes_tree_rev.keys())
            elif not rev_pr:
                return list(self._barcodes_tree_rev[rev_bc].keys())
            elif not for_pr:
                return list(self._barcodes_tree_rev[rev_bc][rev_pr].keys())
            elif not for_bc:
                return list(self._barcodes_tree_rev[rev_bc][rev_pr][for_pr].keys())
            else:
                raise ValueError('ERROR! Use the method get_info_from_bcs() to fetch the information from a set a barcodes and primers')
        else:
            if not for_bc:
                return list(self._barcodes_tree.keys())
            elif not for_pr:
                return list(self._barcodes_tree[for_bc].keys())
            elif not rev_pr:
                return list(self._barcodes_tree[for_bc][for_pr].keys())
            elif not rev_bc:
                return list(self._barcodes_tree[for_bc][for_pr][rev_pr].keys())
            else:
                raise ValueError('ERROR! Use the method get_info_from_bcs() to fetch the information from a set a barcodes and primers')

    def _revcomp(self, seq):
        """self._revcomp(str) -> str
        Returns the reverse complement of a DNA sequence
        """
        return str(Seq(seq).reverse_complement())

    def get_all_for_bc(self, reverse_complement=False):
        """self.get_all_for_bc([bool]) -> list()
        Returns the list of all forward barcodes in the table (as they are or
        rheir reverse complement)
        """
        if reverse_complement:
            return list(map(self._revcomp, self._for_barcodes_in_table))
        else:
            return deepcopy(self._for_barcodes_in_table)

    def get_all_rev_bc(self, reverse_complement=False):
        """self.get_all_rev_bc([bool]) -> list()
        Returns the list of all reverse barcodes in the table (as they are or
        rheir reverse complement)
        """
        if reverse_complement:
            return list(map(self._revcomp, self._rev_barcodes_in_table))
        else:
            return deepcopy(self._rev_barcodes_in_table)

    def get_all_for_pr(self, reverse_complement=False):
        """self.get_all_for_pr([bool]) -> list()
        Returns the list of all forward primers in the table (as they are or
        rheir reverse complement)
        """
        if reverse_complement:
            return list(map(self._revcomp, self._for_primers_in_table))
        else:
            return deepcopy(self._for_primers_in_table)

    def get_all_rev_pr(self, reverse_complement=False):
        """self.get_all_rev_pr([bool]) -> list()
        Returns the list of all reverse primers in the table (as they are or
        rheir reverse complement)
        """
        if reverse_complement:
            return list(map(self._revcomp, self._rev_primers_in_table))
        else:
            return deepcopy(self._rev_primers_in_table)

    def get_info_from_bcs(self, for_bc, for_pr, rev_pr, rev_bc, retrieve='all'):
        """self.get_info_from_bcs(str, str, str, str[, str]) -> dict
        Returns a dictionary (or value if retrieve=specific key) with the
        information for a given set of Barcodes and Primers.
        retrieve = a specific key, or the group values 'all'/'paths'/'info'
        """
        out_d = self._barcodes_tree[for_bc][for_pr][rev_pr][rev_bc]
        if retrieve == 'all':
            return out_d
        elif retrieve == 'paths':
            keys = [k for k in out_d if 'filename' in k]
            out_d = dict([(k, out_d[k]) for k in keys])
            return out_d
        elif retrieve == 'info':
            keys = [k for k in out_d if 'filename' not in k]
            out_d = dict([(k, out_d[k]) for k in keys])
            return out_d
        elif retrieve in out_d:
            return out_d[retrieve]
        else:
            msg = 'ERROR! "{}" is not a valid field to retrieve!'.format(retrieve)
            self._print_log(msg, True)
            raise ValueError(msg)

    def get_dict_from_term(self, term, retrieve='all'):
        """self.get_dict_from_term(str[, str]) -> dict
        Returns a dictionary with all the information for a given term (column
        name in the NGS_barcodes table).
        retrieve = a specific key, or the group values 'all'/'paths'/'info'
        """
        out_d = {}
        for for_bc in self._barcodes_tree:
            for for_pr in self._barcodes_tree[for_bc]:
                for rev_pr in self._barcodes_tree[for_bc][for_pr]:
                    for rev_bc in self._barcodes_tree[for_bc][for_pr][rev_pr]:
                        d = self._barcodes_tree[for_bc][for_pr][rev_pr][rev_bc]
                        if term not in d:
                            msg = 'ERROR! Term "{}" not in NGS_table'.format(term)
                            self._print_log(msg, True)
                            raise ValueError(msg)
                        else:
                            k = d[term]
                            f_bc = d[self._for_bc_col]
                            f_pr = d[self._for_pr_col]
                            r_pr = d[self._rev_pr_col]
                            r_bc = d[self._rev_bc_col]
                            retrieve_d = self.get_info_from_bcs(f_bc, f_pr,
                                                                r_pr, r_bc,
                                                                retrieve)
                            if k in out_d:
                                out_d[k].append(retrieve_d)
                            else:
                                out_d[k] = [retrieve_d]
        return out_d

    def _save2file(self, path, content, empty_file=False):
        """self._save2file(path, str, bool) -> None
        Saves the content into a file in a path. If the file does not exist, it
        creates it. If it exists, the function adds the new information to the
        end of the file if empty_file=False or replaces the content if =True
        """
        if empty_file:
            o = open(path, 'w')
        else:
            o = open(path, 'a')
        o.write(content)
        o.close()


## PROGRAM
if __name__ == '__main__':
    if 'idlelib' not in sys.modules:
        parser = argparse.ArgumentParser(prog='python NGS_table_class.py',
                                             description='Parses and check an NGS_table',
                                             epilog='2018 Leandro Simonetti',
                                             add_help=True,  # allow -h or --help
                                             )
        # Positional arguments
        parser.add_argument('NGS_table',
                            action='store',  # default action
                            type=str,  # this acts as a conversion value
                            nargs='?',  # single argument as str
                            metavar='<path_to_NGS_table>',  # name for help
                            help='path to the input NGS_table file'
                            )
        # Optional arguments
        parser.add_argument('--separator', '-s',  # optional arg with multi options
                            action='store',
                            type=str,
                            nargs='?',
                            default='auto',
                            help='provide the separator for the table or "auto" for the program to attempt autodetection [tab|,|;|etc...|auto] (def=auto)'
                            )

        # parse the arguments
        args = parser.parse_args()

        if not args.NGS_table:
            parser.print_help()
            exit()

        ngs_table_path = args.NGS_table
        separator = args.separator
        if separator.lower() == 'tab':
            separator = '\t'

        lib_dic = None
        graphic_format = 'png'
        log = None

    else:
        from NGS_library_class import NGS_library
        libs = [('HD', 'Human Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD.tsv'),
                ('HD2', 'Human Disorderome ver 2 - all pools', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2.tsv'),
                ('HD2_cytoplasm', 'Human Disorderome ver 2 - cytoplasm', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_cytoplasm.tsv'),
                ('HD2_n&c', 'Human Disorderome ver 2 - nucleus & cytoplasm', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_n&c.tsv'),
                ('HD2_nucleus', 'Human Disorderome ver 2 - nucleus', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_nucleus.tsv'),
                ('HD2_endomembrane', 'Human Disorderome ver 2 - endomembrane', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_endomembrane.tsv'),
                ('VD', 'Viral Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/VD.tsv'),
                ('PhosLib', 'mimetic-propPD', '3', 'AGGAGCCTTTAATTGTATCGGT', 'TCCGCCACCGGGGGCGCT', 27, 2, '../libraries/PhosLib.tsv'),
                ('BD', 'Bacterial Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/BD.tsv'),
                ]
        lib_dic = {}
        for lib_id, lib_name, lib_ver, f_pr, r_pr, oligo_length, stop, path in libs:
            lib_dic[lib_id] = {'id'            : lib_id,
                               'name'          : lib_name,
                               'version'       : lib_ver,
                               'forward_primer': f_pr,
                               'reverse_primer': r_pr,
                               'oligo_length'  : oligo_length,
                               'aa_length'     : oligo_length / 3,
                               'stop_length'   : stop * 3,
                               'path'          : path,
                               }
        ngs_table_path = '../181003_M01548_0191_000000000-BJDK9/P11353/P11353_NGS_Barcodes.tsv'
        separator = 'auto'
        graphic_format = 'png'
        log = '../test_log.txt'

    #
    print('IMPORTING NGS_TABLE...')
    t = NGS_table(ngs_table_path, 'P11353', lib_dic, separator, graphic_format,
                  log, let_me_be=True)
    print('... DONE!\n')
    t.info()
