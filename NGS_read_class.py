#!/usr/bin/env python
# By Leandro Simonetti
# Shared under XX
#
# Script:       NGS_read_class
# Version:      1
# Last Edit:    2018-12-18
desc_text= '''
The read class takes as input a read or two from the FASTQ files (depending on
if it's a single or paired reads), a Barcodes table object, and demultiplexes
the read. This implies * steps:
1) Check the average quality of the read (default cut-off = 20)
2) Searches for the barcodes and adapters (default mismatch cut-off = 1 for each)
3) Checks that the barcodes (or adapters?) are not ambiguous
If the demultiplexing can be done (good quality sequence, unambiguous barcodes
and adaptors are identified), the class gets the trimmed sequence, it's length
and the data + researcher paths for the read.
'''
# Functions:
#
# Objects:
#     NGS_read
# Commandline:
#
# Dependencies:
#     General: os, sys, gzip, time, itertools, biopython
#     Custom: NGS_table_class

##  VARIABLES
ngs_table_path = './data/MAXIM_NGS_Barcodes.tsv'
for_fastq_path = './data/MAXIM_FOR_READS.fastq.gz'
rev_fastq_path = './data/MAXIM_REV_READS.fastq.gz'

verbose = False


## MODULES
import os, sys, gzip, time
from Bio.Seq import Seq
from NGS_table_class import NGS_table

if sys.version_info.major >= 3:
    print('Python 3.x detected')
    from itertools import zip_longest as izip
    decode = True
else:
    print('Python 2.x detected')
    input = raw_input
    from itertools import izip_longest as izip
    decode = False


## CLASSES
class NGS_read(object):
    # Attributes
    ok_good_quality  = False  # True if the average quality of for OR rev > self._qty_cutoff
    ok_no_ambiguity  = False  # True if barcodes have no ambiguity
    ok_demultiplexed = False  # True if demultiplexing succeeds
    _reverse = False  # True if there's input data for the reverse seq and qty
    _for_qty_avg = 0
    _rev_qty_avg = 0
    _process_for = False  # True if the forward read qty > self._qty_cutoff
    _process_rev = False  # True if the reverse read qty > self._qty_cutoff
    forward_barcode = None
    forward_primer  = None
    reverse_primer  = None
    reverse_primer_position = None
    reverse_barcode = None
    trimmed_sequence = None
    path_data       = None
    path_researcher = None

    # Methods
    def __init__(self, name, for_seq, for_qty, ngs_table,
                 qty_cutoff=20, bc_mmatch_cutoff=1, pr_mmatch_cutoff=1,
                 rev_seq=None, rev_qty=None):
        # create attributes
        self.name = name
        self._for_seq = for_seq
        self._for_qty = for_qty
        self._rev_seq = rev_seq
        self._rev_qty = rev_qty
        self._ngs_table = ngs_table
        self._qty_cutoff =  qty_cutoff
        self._bc_mmatch_cutoff = bc_mmatch_cutoff
        self._pr_mmatch_cutoff = pr_mmatch_cutoff
        # create mutable attributes
        self._for_qty_list = []
        self._rev_qty_list = []
        # process data
        self._process()

    def info(self):
        """
        Outputs instance information
        """
        print('Read_name: {}'.format(self.name))
        print('Forward_sequence = {}'.format(self._for_seq))
        print('Reverse_sequence = {}'.format(self._rev_seq))
        print('Forward_avg_qty  = {}'.format(self._for_qty_avg))
        print('Reverse_avg_qty  = {}'.format(self._rev_qty_avg))
        print('DEMULTIPLEXING:')
        print('\tQuality     = {}'.format('PASS' if self.ok_good_quality else 'FAIL'))
        print('\tAmbiguity   = {}'.format('PASS' if self.ok_no_ambiguity else 'FAIL'))
        print('\tDemultiplex = {}'.format('PASS' if self.ok_demultiplexed else 'FAIL'))
        print('FOUND BARCODES/PRIMERS')
        print('\tForward_barcode = {}'.format(self.forward_barcode))
        print('\tForward_primer  = {}'.format(self.forward_primer))
        print('\tReverse_primer  = {}'.format(self.reverse_primer))
        print('\tReverse_barcode = {}'.format(self.reverse_barcode))
        if self.ok_demultiplexed:
            print('OUTPUT DATA:')
            print('\tData_path       = {}'.format(self.path_data))
            print('\tResearcher_path = {}'.format(self.path_researcher))
            print('\tTrimmed_seq     = {}'.format(self.trimmed_sequence))
            print('\tTrimmed_seq_len = {}'.format(self.trimmed_sequence_length))

    def _process(self):
        """
        Processes the input variables
        """
        # get the qualities
        self._check_qualities()
        # if the qualities pass the cut-off, then demultiplex
        if self._process_for:  # or self._process_rev:  # <<< I'm only working with for_reads from now on
            self._demultiplex_read()
            # if the demultiplexing was successful, clean-up the read and fetch paths
            if self.ok_demultiplexed:
                self._trim()
                self._get_paths()

    def _check_qualities(self):
        """
        Transforms the forward and reverse reads qualities to numerical values
        and calculates their average quality
        """
        # check if there's input for the reverse
        if self._rev_seq and self._rev_qty:
            self._reverse = True
        # process the qualities
        self._for_qty_avg, self._for_qty_list = self._convert_quality(self._for_qty)
        if self._reverse:
            self._rev_qty_avg, self._rev_qty_list = self._convert_quality(self._rev_qty)
        self._process_for = self._for_qty_avg >= self._qty_cutoff
        self._process_rev = self._rev_qty_avg >= self._qty_cutoff
        if self._process_for or self._process_rev:
            self.ok_good_quality = True

    def _convert_quality(self, quality):
        """self._convert_quality(str) -> tuple(float, list)
        Convert quality chars to numbers (mapped from 33[->126] as 0->93)
        Returns a tuple with:
            (average quality, list of quality per position values)
        """
        quality = [ord(c)-33 for c in quality]
        return(sum(quality)/float(len(quality)), quality)

    def _demultiplex_read(self):
        """
        Demultiplexes (finds barcodes and primers) the read
        """
        # finding the forward barcode is easy
        for_barcodes = self._ngs_table.get_all_for_bc()
        f_bc_max_len = max(map(len, for_barcodes))  # get the length of the longest barcode(s)
        f_bc = self._for_seq[:f_bc_max_len]  # get the "region" that can contain the barcode
        f_bc = self._map_seq(f_bc, for_barcodes,
                             mm_cutoff=self._bc_mmatch_cutoff
                             )
        # if we find 0 (or only 1) barcode then there's no ambiguity
        if f_bc[0] == 0:
            self.ok_no_ambiguity = True
        elif f_bc[0] == 1:
            f_bc = f_bc[2][0]
            self.forward_barcode = f_bc
            #now to find the forward primer
            for_primers = self._ngs_table.get_next_level_values(for_bc=f_bc)  # get the possible for_primers for the found for_barcode
            f_pr_max_len =  max(map(len, for_primers))  # get the max primer length
            f_pr = self._for_seq[len(f_bc):len(f_bc) + f_pr_max_len]  # get the region that should contain the primer
            f_pr = self._map_seq(f_pr, for_primers,
                                 mm_cutoff=self._pr_mmatch_cutoff
                                 )
            # ambiguity time
            if f_pr[0] == 0:
                self.ok_no_ambiguity = True
            elif f_pr[0] == 1:
                f_pr = f_pr[2][0]
                self.forward_primer =  f_pr
                # to find the reverse primer
                rev_primers = self._ngs_table.get_next_level_values(for_bc=f_bc,
                                                                    for_pr=f_pr
                                                                    )
                # this is more complex and we need to search for it in the
                # sequence, to reduce the search space we can cut out the
                # for_bc and for_pr regions:
                ini = len(f_bc) + len(f_pr)
                #get the reverse primer position
                rev_primer_list = []
                for r_pr in rev_primers:
                    r_pr_pos = self._scan_seq(r_pr, self._for_seq,
                                              start_index=ini,
                                              mm_cutoff=self._pr_mmatch_cutoff,
                                              from_right=True,
                                              seq_target_qty=self._for_qty,
                                              qty_cutoff=self._qty_cutoff
                                              )
                    if r_pr_pos:
                        rev_primer_list.append([r_pr, r_pr_pos])
                # ambiguity time
                if len(rev_primer_list) == 0:
                    self.ok_no_ambiguity = True
                elif len(rev_primer_list) == 1:
                    r_pr, r_pr_pos = rev_primer_list[0]
                    self.reverse_primer = r_pr
                    self.reverse_primer_position = r_pr_pos
                    # get the reverse barcode
                    rev_barcodes = self._ngs_table.get_next_level_values(for_bc=f_bc,
                                                                         for_pr=f_pr,
                                                                         rev_pr=r_pr
                                                                         )
                    r_bc_min_len = min(map(len, rev_barcodes))
                    r_bc_max_len = max(map(len, rev_barcodes))
                    r_bc = self._for_seq[r_pr_pos + len(r_pr):r_pr_pos + len(r_pr) + r_bc_max_len]  # get the region
                    # discard too short barcode region, otherwise map it
                    if len(r_bc) < r_bc_min_len:
                        r_bc = [0]
                    else:
                        r_bc = self._map_seq(r_bc, rev_barcodes,
                                             mm_cutoff=self._pr_mmatch_cutoff
                                             )
                    # last ambiguity time!
                    if r_bc[0] == 0:
                        self.ok_no_ambiguity = True
                    elif r_bc[0] == 1:
                        r_bc = r_bc[2][0]
                        self.reverse_barcode = r_bc
                        self.ok_no_ambiguity = True
                        self.ok_demultiplexed = True
                    else:
                        self.reverse_barcode = 'Ambiguous ({})'.format('|'.join(r_bc[2]))
                else:
                    self.reverse_primer = 'Ambiguous ({})'.format('|'.join(rev_primer_list))
            else:
                self.forward_primer = 'Ambiguous ({})'.format('|'.join(f_pr[2]))
        else:
            self.forward_barcode = 'Ambiguous ({})'.format('|'.join(f_bc[2]))

    def _map_seq(self, bc_seq, bc_list, mm_cutoff=0):
        """self._map_seq(str, list, int) -> [int, str, list(str)]
        Maps an input barcode sequence to a list of barcodes and returns a list
        containing:
            [number of correct matches, input bc, [possible mapped barcodes list]]
        NOTE:
        If there's a perfect match, then it returns that one, otherwise it runs
        self._match() with the mm_cutoff for every barcode in bc_list
        """
        # check the input for errors
        if len(bc_list) != len(set(bc_list)):
            raise IOError('The list of input barcodes contain repeated values.')
        # if the bc_seq exist in the list, return that one
        if bc_seq in bc_list:
            return [1, bc_seq, [bc_seq]]
        # otherwise, check the bc against the list
        out = [0, bc_seq, []]
        for s in bc_list:
            if self._match(bc_seq, s, mm_cutoff):
                out[0] += 1
                out[2].append(s)
        return out

    def _match(self, seq, target, mm_cutoff=0, ignore=''):
        """self._match(str, str, int) -> bool
        Compares two sequences and returns True or False depending on if the
        mismatches are below or above the cut-off. It's possible to ignore certain
        character when comparing (for example '-' as gaps)
        The 'target' can be shorter than 'seq', but not the other way around.
        """
        mm_cutoff = int(mm_cutoff)
        errors = 0
        for i in range(len(target)):
            if errors > mm_cutoff:
                return False
            elif seq[i] != target[i] and \
                seq[i] not in ignore and \
                target[i] not in ignore:
                errors += 1
        if errors > mm_cutoff:
            return False
        return True

    def _scan_seq(self, seq_bait, seq_target, start_index=0, mm_cutoff=0,
                  from_right=True, seq_target_qty=None, qty_cutoff=None):
        '''self._scan_seq(str, str [, int][, int][, bool][, str, int]]) -> None/int
        Searches for the rightmost position of a sequence (seq_bait) in a longer
        sequence (seq_target). It allows for a mismatch cut-off between the bait
        and the sequence region, and also a quality cut-off for the region of
        the target sequence to to even compare it to the bait (if a qty_cutoff
        is provided, the sequence of the quality must be provided too). If the
        bait is found, it returns its index position, otherwise it return None
        '''
        # split the seq_target as if a window of seq_bait length was
        # traversing it 1 base by 1
        frag_list = self._splitted(seq_target, len(seq_bait), start_index,
                                   len(seq_target)
                                   )
        # List of the average qualities of the seq_target fragments
        if qty_cutoff:
            q_list = self._splitted(seq_target_qty, len(seq_bait), start_index,
                                    len(seq_target_qty)
                                    )
            q_list = [i[1] for i in q_list]
            q_list = [i[0] for i in list(map(self._convert_quality, q_list))]
        # Now the tests
        indexes = list(range(len(frag_list)))
        if from_right:
            indexes.reverse()
        for n in indexes:
            fragment = frag_list[n]
            if qty_cutoff:
                q = q_list[n] > qty_cutoff
            else:
                q = True
            #
            if q:
                if self._match(seq_bait, fragment[1], mm_cutoff):
                    return fragment[0]
        return None

    def _splitted(self, seq, size, start_index, end_index):
        '''self._splitted(str, int, int, int) -> list([int, str])
        Return a list of sequences as if a sliding window of a determined size
        with step = 1 was run on the given seq
        '''
        out = []
        for n in range(start_index, end_index - size + 1):
            out.append([n, seq[n:n+size]])
        return out

    def _trim(self):
        """
        Trim the DNA sequence
        """
        ini = len(self.forward_barcode) + len(self.forward_primer)
        end = self.reverse_primer_position
        self.trimmed_sequence = self._for_seq[ini:end]  # WARNING! If rev_seq, this probably should be a consensus
        self.trimmed_sequence_length = len(self.trimmed_sequence)

    def _get_paths(self):
        """
        Fetch the paths from the NGS_table
        """
        paths = self._ngs_table.get_info_from_bcs(self.forward_barcode,
                                                  self.forward_primer,
                                                  self.reverse_primer,
                                                  self.reverse_barcode
                                                  )
        self.path_data = paths['data_filename']
        self.path_researcher = paths['researcher_filename']


## FUNCTIONS
def fastq_parser_generator(file_for_path, file_rev_path=None, decode=False):
    """fastq_parser_generator(path [, path] [, bool]) -> generator
    The function takes in the path (or paths) of fastq file(s) and returns a
    generator that parses the sequences into a list of 2 tuples:
        [(for_name, for_sequence, for_quality), (rev_name, rev_sequence, rev_quality)]
    If file_rev_path is not provided, then the rev values are = None
    Decode is necessary for Python 3.x
    """
    with gzip.open(file_for_path) as file_for_path:
        if file_rev_path:
            with gzip.open(file_rev_path) as file_rev_path:
                for read_f, read_r in zip(izip(*[file_for_path]*4), izip(*[file_rev_path]*4)):
                    for_read = read_list_parseNclean(read_f, decode)
                    rev_read = read_list_parseNclean(read_r, decode)
                    yield [for_read, rev_read]
        else:
            for read_f in izip(*[file_for_path]*4):
                yield [read_list_parseNclean(read_f, decode),
                       (None, None, None)
                       ]

def read_list_parseNclean(read_list, decode=False):
    """read_list_parseNclean(list) -> list
    Does some cleaning with the fastq read in list format:
    - decodes each component (for Python 3.x when gzip is used)
    - strips each component
    - removes the "+" component
    """
    if decode:
        read_list = list(map(f_decode, read_list))
    out = list(map(f_strip, read_list))
    out.pop(2)
    return tuple(out)

def f_decode(obj):
    """f_decode(2.x=string / 3.x=bite) -> 2.x=unicode / 3.x=string
    Converts the method .decode() into a functon, so map() can be used
    """
    return obj.decode().strip()

def f_strip(string):
    """f_strip(str) -> str
    Converts the method .strip() into a functon, so map() can be used
    """
    return string.strip()


## PROGRAM
if __name__ == '__main__':
    # libraries dictionary
    libs = [('HD', 'Human Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD.tsv'),
            ('HD2', 'Human Disorderome ver 2 - all pools', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2.tsv'),
            ('HD2_cytoplasm', 'Human Disorderome ver 2 - cytoplasm', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_cytoplasm.tsv'),
            ('HD2_n&c', 'Human Disorderome ver 2 - nucleus & cytoplasm', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_n&c.tsv'),
            ('HD2_nucleus', 'Human Disorderome ver 2 - nucleus', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_nucleus.tsv'),
            ('HD2_endomembrane', 'Human Disorderome ver 2 - endomembrane', '2', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/HD2_endomembrane.tsv'),
            ('VD', 'Viral Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/VD.tsv'),
            ('BD', 'Bacterial Disorderome', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/BD.tsv'),
            ('PhosLib', 'mimetic-propPD', '3', 'AGGAGCCTTTAATTGTATCGGT', 'TCCGCCACCGGGGGCGCT', 27, 2, '../libraries/PhosLib.tsv'),
            ('smORFINT', 'smORF_internal library', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/smORFINT.tsv'),
            ('Rand', 'Random Library', '1', 'TATGCAGCCTCTTCATCTGGC', 'GGTGGAGGATCCGGAGG', 48, 0, '../libraries/Rand.tsv'),
            ]
    lib_dic = {}
    for lib_id, lib_name, lib_ver, f_pr, r_pr, oligo_length, stop, path in libs:
        lib_dic[lib_id] = {'id'            : lib_id,
                           'name'          : lib_name,
                           'version'       : lib_ver,
                           'forward_primer': f_pr,
                           'reverse_primer': r_pr,
                           'oligo_length'  : oligo_length,
                           'aa_length'     : oligo_length / 3,
                           'stop_length'   : stop * 3,
                           'path'          : path,
                           }
    # load the NGS_table
    print('\nIMPORTING NGS_TABLE...')
    ngs_table = NGS_table(ngs_table_path, 'QL-1686', lib_dic, let_me_be=True)
    print('... DONE!\n')
    ngs_table.info()
    # establish some counters to check the output
    c = {'total_reads': 0,
         'trashed': 0,
         'ambiguous': 0,
         'not_demultiplexed': 0,
         'demultiplexed': 0,
         # more detail
         'no_for_bc': 0,
         'no_for_pr': 0,
         'no_rev_pr': 0,
         'no_rev_bc': 0,
         }
    # timing
    if not verbose:
        start_time = time.time()
    # process the reads
    print('\nPROCESSING THE FASTQ FILE(s)...')
    fastq = fastq_parser_generator(for_fastq_path, rev_fastq_path, decode)
    for fastq_read in fastq:
        for_name = fastq_read[0][0]
        for_seq = fastq_read[0][1]
        for_qty = fastq_read[0][2]
        rev_name = fastq_read[1][0]
        rev_seq = fastq_read[1][1]
        rev_qty = fastq_read[1][2]
        # check that the forward and reverse reads have the same name
        fix_for_name = '>' + for_name.split(' ')[0][1:]  # removes extra info after a space and changes the "@" in the beginning for a ">"
        if rev_name:
            fix_rev_name = '>' + rev_name.split(' ')[0][1:]
            if fix_for_name != fix_rev_name:
                raise IOError('ERROR! Read names do not match ({} != {})'.format(fix_for_name, fix_rev_name))
        # create the read object
        read = NGS_read(fix_for_name, for_seq, for_qty, ngs_table,
                        rev_seq=rev_seq, rev_qty=rev_qty
                        )
        if verbose:  # to go read by read
            read.info()
            input('')
        # count!
        c['total_reads'] += 1
        if not read.ok_good_quality:
            c['trashed'] += 1
        elif not read.ok_no_ambiguity:
            c['ambiguous'] += 1
        elif not read.ok_demultiplexed:
            c['not_demultiplexed'] += 1
            if not read.forward_barcode:
                c['no_for_bc'] += 1
            elif not read.forward_primer:
                c['no_for_pr'] += 1
            elif not read.reverse_primer:
                c['no_rev_pr'] += 1
            elif not read.reverse_barcode:
                c['no_rev_bc'] += 1
        elif read.ok_demultiplexed:
            c['demultiplexed'] += 1
    # more timing
    if not verbose:
        total_time = time.strftime("%H:%M:%S", time.gmtime(time.time() - start_time))
        print('... DONE! (elapsed time = {})'.format(total_time))
    else:
        print('... DONE!')
    # print results
    print('\nRESULTS OF THE DEMULTIPLEXING:')
    print('\tTotal_reads     = {}'.format(c['total_reads']))
    print('\tDemultiplexed   = {}'.format(c['demultiplexed']))
    print('\tLow_quiality    = {}'.format(c['trashed']))
    print('\tAmbiguities     = {}'.format(c['ambiguous']))
    print('\tBC/Pr_not_found = {}'.format(c['not_demultiplexed']))
    print('\t    > No_for_bc = {}'.format(c['no_for_bc']))
    print('\t    > No_for_pr = {}'.format(c['no_for_pr']))
    print('\t    > No_rev_pr = {}'.format(c['no_rev_pr']))
    print('\t    > No_rev_bc = {}'.format(c['no_rev_bc']))
    # bye bye!
    print('\nPROCESS FINISHED!!!')
